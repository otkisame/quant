def choose(n,k):
	return np.prod(np.arange(n,n-k,-1)/np.arange(k,0,-1))

def psiWP(E1,E2,n):
    E = np.linspace(E1,E2,n)
    b = np.array([choose(n-1,i)/2**n for i in np.arange(n)]) 
    k = np.sqrt(E)
    return lambda x,t: np.sum([b[l]*np.exp(-1j*k[l]*x)*np.exp(-1j*E[l]*t) for l in np.arange(n)], axis=0)

def animPsi(x1,x2,E1=5,E2=15,n=3,tmax=100,dx=0.01,dt=0.01,mode='r'):
    
    psi = psiWP(E1,E2,n)
    x = np.linspace(x1,x2,int((x2-x1)/dx))
        
    lbl = "Time development of"
    if mode == 'r':
        f = lambda t: np.real(psi(x,t))
        lbl +=r" Re Ψ"
    if mode == 'i':
        f = lambda t: np.imag(psi(x,t))
        lbl +=r" Im Ψ"
    if mode == 'm':
        f = lambda t: np.abs(psi(x,t))**2
        lbl +=r" |Ψ|²"
    if mode == 'a':
        f = lambda t: np.angle(psi(x,t))
        lbl +=r" ag Ψ"
    
    tm_tmpl="t = %03.2f"
    y1 = np.amin(f(0))
    y2 = np.amax(f(0))
    
    fig,ax = plt.subplots()
    line, = ax.plot(x,f(0))
    tm_x = 0.45
    tm_y = -0.10
    tm_txt=ax.text(tm_x,tm_y,'',transform=ax.transAxes)

    def init():
        ax.set_xlim(x1,x2)
        ax.set_ylim(y1,y2)
        return line,
    
    def animate(t):
        line.set_ydata(f(t))
        tm_txt.set_text(tm_tmpl % t)
        return line, tm_txt

    ani = anim.FuncAnimation(fig,animate,frames=np.arange(0,tmax+dt,dt),init_func=init,interval=10,repeat=False,blit=True,save_count=1500)
    plt.title(lbl)
    plt.grid(True)
    plt.show()
    return ani

